const express = require('express');
const port = require('./port');
const app = express();
const setup = require('./middlewares/setup');
const HOST = '0.0.0.0';

setup(app, {});

app.listen(port, HOST, () => {
  /* eslint-disable-next-line */
  console.log(`Server started at ${port}`);
});
