const productionMiddleware = require('./production');
const developmentMiddleware = require('./development');

module.exports = (app, options) => {
  const isProd = process.env.NODE_ENV === 'production';
  if (isProd) {
    productionMiddleware(app, options);
  } else {
    developmentMiddleware(app, options);
  }

  return app;
};
