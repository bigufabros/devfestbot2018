const path = require('path');
const express = require('express');
const compression = require('compression');

module.exports = app => {
  app.use(compression());
  app.use(express.static(path.resolve('build')));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve('build', 'index.html'));
  });
};
