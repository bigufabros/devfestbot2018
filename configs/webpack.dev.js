const webpack = require('webpack');
const baseConfig = require('./webpack.base');
const config = baseConfig({
  mode: 'development',
});

config.entry = [
  'webpack-hot-middleware/client?path=/__webpack_hmr&reload=true',
  ...config.entry,
];

config.plugins.push(new webpack.HotModuleReplacementPlugin());

module.exports = config;
