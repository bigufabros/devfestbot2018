const baseConfig = require('./webpack.base');
const config = baseConfig({
  mode: 'production',
});

module.exports = config;
