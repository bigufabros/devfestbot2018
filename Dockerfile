FROM node:10-alpine

LABEL maintainer="kazakov.developer@gmail.com"

WORKDIR /var/www

COPY package*.json ./

RUN npm install

COPY ./configs ./configs

COPY ./app ./app

COPY ./server ./server

EXPOSE 3000

CMD [ "npm", "run", "start:production" ]
